<?php
/**
 * Created by PhpStorm.
 * User: jefinghelbrecht
 * Date: 24/10/18
 * Time: 20:09
 */
?>
<main class="index">
    <div class="tile" id="Speler"><h1><a href="/Player/Editing">Speler</a></h1></div>
    <div class="tile" id="Team"><h1><a href="/Team/Editing">Team</a></h1></div>
    <div class="tile" id="Wedstrijd"><h1><a href="/Wedstrijd/Editing">Wedstrijd</a></h1></div>
    <div class="tile" id="Liga"><h1><a href="/Liga/Editing">Liga</a></h1></div>
</main>