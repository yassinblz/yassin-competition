<?php
/**
 * Created by PhpStorm.
 * User: jefinghelbrecht
 * Date: 24/10/18
 * Time: 21:37
 */
namespace ModernWays\MVC;

class Routing
{
    private $entity;
    private $action;

    /**
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    public function init()
    {
        echo $_SERVER['SCRIPT_NAME'];
        $pos = strrpos($_SERVER['SCRIPT_NAME'], '/');
        $path = substr($_SERVER['SCRIPT_NAME'], 0, $pos + 1);

        $uc = strtolower(str_replace($path, '', $_SERVER['REDIRECT_URL']));
        $ucArray = explode('/' , $uc);
        $this->entity = $ucArray[0];
        $this->action = $ucArray[1];
    }

    public static function watIsHetNutVanStatic()
    {
        echo 'je moet geen instantie van de klasse maken bbb';
    }
}